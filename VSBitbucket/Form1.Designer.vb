﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnBackground = New System.Windows.Forms.Button()
        Me.btnGrow = New System.Windows.Forms.Button()
        Me.btnShrink = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnBackground
        '
        Me.btnBackground.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnBackground.Location = New System.Drawing.Point(54, 50)
        Me.btnBackground.Name = "btnBackground"
        Me.btnBackground.Size = New System.Drawing.Size(176, 61)
        Me.btnBackground.TabIndex = 0
        Me.btnBackground.Text = "Change Background Color"
        Me.btnBackground.UseVisualStyleBackColor = True
        '
        'btnGrow
        '
        Me.btnGrow.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnGrow.Location = New System.Drawing.Point(54, 127)
        Me.btnGrow.Name = "btnGrow"
        Me.btnGrow.Size = New System.Drawing.Size(176, 34)
        Me.btnGrow.TabIndex = 1
        Me.btnGrow.Text = "Grow"
        Me.btnGrow.UseVisualStyleBackColor = True
        '
        'btnShrink
        '
        Me.btnShrink.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnShrink.Location = New System.Drawing.Point(54, 177)
        Me.btnShrink.Name = "btnShrink"
        Me.btnShrink.Size = New System.Drawing.Size(176, 34)
        Me.btnShrink.TabIndex = 2
        Me.btnShrink.Text = "Shrink"
        Me.btnShrink.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.SlateGray
        Me.ClientSize = New System.Drawing.Size(284, 261)
        Me.Controls.Add(Me.btnShrink)
        Me.Controls.Add(Me.btnGrow)
        Me.Controls.Add(Me.btnBackground)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(350, 350)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(300, 300)
        Me.Name = "Form1"
        Me.Text = "VSBitbucket"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnBackground As Button
    Friend WithEvents btnGrow As Button
    Friend WithEvents btnShrink As Button
End Class
